var express = require('express');
var port = process.env.PORT || 8080;
var app = express();

app.get('/', function(request, response) {
    response.sendFile(__dirname + '/index.html');
});

app.use('/img', express.static(__dirname + '/img'));
app.use('/css', express.static(__dirname + '/css'));

app.listen(port);
