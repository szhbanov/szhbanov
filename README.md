# This is the Developer`s personal web page

Once, I listened to a front-end developer`s speech at a conference about using the states of input elements and I really liked the idea.
Today we often use Javascript to implement DOM manipulations such as the tab switching or the pop-up window appearance.
But it is possible to do that using CSS rules!

I decided to make a single-page website which contains some subpages and a language switcher.

I used radio buttons with labels.
It is very useful because an input element can lie on top of the page structure but its label can be on the menu.
In that case the input element can affect on the elements below it (I use ~ in CSS rules).

So, I developed an SP website **without any frameworks and Javascript code**.

## How it works

HTML
```html
<input type="radio" name="page-state" id="state_1">
<input type="radio" name="page-state" id="state_2">
... some html like this ...
<div class="menu">
  <label class="menu-item" for="state_1">State 1</label>
  <label class="menu-item" for="state_2">State 2</label>
</div>
...
<div class="page state_1">Lorem ipsum...</div>
<div class="page state_2">Dolor color...</div>
```
CSS
```css
.page {
  display: none;
}
#state_1 ~ .state_1,
#state_2 ~ .state_2 {
  display: block;
}
```
https://www.w3.org/wiki/CSS/Selectors/combinators/general

https://www.w3.org/wiki/CSS/Selectors/combinators/adjacent
