const gulp = require('gulp');
const less = require('gulp-less');
const gulpif = require('gulp-if');
const util = require('gulp-util');
const sourcemaps = require('gulp-sourcemaps');
const watch = require('gulp-watch');
const replace = require('gulp-replace');

const devMode = util.env.env === 'dev';
const sourceDir = 'Source';
const outDir = 'Build';
const buildTime = new Date().getTime();
const linkRegex = /\"(.*?(\.(jpg|png|svg|css)){1})\"/g;

gulp.task('build', [ 'copy', 'less']);

gulp.task('less', () => processStyles());

gulp.task('copy', function(){
  return gulp.src(
    [
      `${sourceDir}/*img/**/*`,
      `${sourceDir}/*`,
      `!${sourceDir}/styles`
    ])
    .pipe(replace(linkRegex, '$1?_=' + buildTime, {skipBinary:true}))
    .pipe(gulp.dest(outDir))
});
//+++++++++++++++++++++++++++++++++++++++++++++++++

function processStyles(isWatcher = false) {
  return gulp.src(
    [
      `${sourceDir}/styles/*.less`,
      `${sourceDir}/styles/*.css`
    ])
    .pipe(gulpif(devMode, sourcemaps.init()))
    .pipe(less({
      compress: true,
      cleancss: true
    }))
    .on('error', function(err) {
      util.log('ERROR: ' + err.message);
      isWatcher || process.exit(1);
    })
    .on('end', () => isWatcher && util.log('... styles rebuild complete' + isWatcher))
    .pipe(replace(linkRegex, `"$1?_=${buildTime}"`, {skipBinary:true}))
    .pipe(gulpif(devMode, sourcemaps.write('./')))
    .pipe(gulp.dest(`${outDir}/css`));
}
